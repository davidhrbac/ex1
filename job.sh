#!/bin/bash
export MODULEPATH=$MODULEPATH:~/.local/easybuild/modules/all
ml PETSc/3.14.4-foss-2020b 
echo "----------------------------------------------------"
echo "Job ordering:  $ORDER"
echo "Job processes: $COUNT"
echo "----------------------------------------------------"
echo "Loaded modules:"
ml
echo "----------------------------------------------------"
cd ~/ex1
mpiexec -c $COUNT  ./ex1  -f ../matrixes/databin/dielFilterV2real.bin -t $ORDER -log_view
