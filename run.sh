declare -a arr=('amd' 'natural' 'nd' '1wd' 'qmd' 'rcm' 'rowlength'  'spectral' 'wbm')

for i in "${arr[@]}"
do
   echo "$i"
   qsub -A DD-21-16 -q qprod -l select=1:ncpus=36:mpiprocs=36 -l walltime=00:30:00 -v ORDER="$i",COUNT="1" -N "$i-1" ~/ex1/job.sh
done

